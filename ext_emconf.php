<?php

/**
 * Extension Manager/Repository config file for ext "laroma".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Laroma Sitepackage',
    'description' => 'Laroma Matratzenmanufaktur Site Package',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'bootstrap_package' => '11.0.0-11.0.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Pixelanker\\Laroma\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Christian Hansen',
    'author_email' => 'christian@herrhansen.com',
    'author_company' => 'Pixelanker',
    'version' => '1.0.0',
];
