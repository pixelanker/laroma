console.log('Made by Pixelanker with ♥.');

var rem = function rem() {
    var html = document.getElementsByTagName('html')[0];
    return function () {
        return parseInt(window.getComputedStyle(html)['fontSize']);
    }
}();

var resize = function resize() {
    $('.card-menu .card-body').css('margin-top', function () {
        return '-' + ($(this).height() / rem() + 1.75) + 'rem';
    });
}

// MegaMenu
// Prevent closing from click inside dropdown
$(document).on('click', '.dropdown-menu.megamenu', function (e) {
    e.stopPropagation();
});

// wrap second word in strong-tags
(function ($) {
    $.fn.extend({
        strengthenSecondWord: function () {
            this.each(function () {
                var $this = $(this);
                var headerParts = $this.text().trim().split(" ");
                $this.html(headerParts[0] + ' <strong>' + headerParts[1] + '</strong>');
            });
        }
    })
}(jQuery));

// second word in megamenu-header bold
$('.megamenu .product .dropdown-text').strengthenSecondWord();
$('.page-1 .card-menu:first-child .card-title').strengthenSecondWord();

// Mega-menu
$('.has-megamenu').mouseenter(function () {
    $('#mainnavigation').addClass('mainnavigation-dark');
}).mouseleave(function () {
    $('#mainnavigation').removeClass('mainnavigation-dark');
});

// mobile menu
$('.navbar-toggler').click(function () {
    if ($(this).hasClass('collapsed')) {
        $('.header-meta').css('opacity', '1');
        $('.navbar-mainnavigation').addClass('nav-bg-white');
    } else {
        $('.header-meta').css('opacity', '0');
        $('.navbar-mainnavigation').removeClass('nav-bg-white');
    }
})

$('.meta-search').click(function () {
    $('#search-overlay').fadeIn(300);
    //$(this).next('.meta-search-form').toggle();
});

$('#search-popup-close').click(function () {
    $('#search-overlay').fadeOut(300);
});


$(window).resize(function(){
    console.log('resize');
    resize();
});

// initial call
$(document).ready(function (){
    resize();
});

$(document).ready(function(){
    setInterval(function(){
    var x = __cmp('getCMPData');
    if ($('.cmplazypreviewiframe').css('display') === 'none') {
        $('.embed-responsive').removeClass('no-consent');
    } else {
    if(x !== undefined && "vendorConsents" in x)
    {
    if("s30" in x.vendorConsents && x.vendorConsents["s30"])
    {
        $('.embed-responsive').removeClass('no-consent');
    }
    else
    {
        $('.embed-responsive').addClass('no-consent');
    }
    } else {
        $('.embed-responsive').addClass('no-consent');
    }
}
    }, 1000)
})
